<?php

/**
 * @file
 * Contain database function for multiple sitemap.
 */

/**
 * Save records into database.
 *
 * @param array $input
 *   Having input data.
 * @param int $update_ms_id
 *   Multiple sitemap id.
 *
 * @return int
 *   Ms id.
 */
function multiple_sitemap_save_record($input = array(), $update_ms_id = NULL) {
  $ms_id = NULL;
  if (is_null($update_ms_id) && !empty($input)) {
    // Insert new record.
    try {
      $ms_id = db_insert('multiple_sitemap')
        ->fields(array(
          'file_name' => $input['file_name'],
          'custom_links' => $input['custom_links'],
        ))
        ->execute();
    }
    catch (Exception $e) {
      drupal_set_message(t('File name already exist'), 'error');
    }
  }
  else {
    // Update record.
    try {
      $ms_id = db_update('multiple_sitemap')
        ->fields(array(
          'file_name' => $input['file_name'],
          'custom_links' => $input['custom_links'],
        ))
        ->condition('ms_id', $update_ms_id, '=')
        ->execute();

      $ms_id = $update_ms_id;
    }
    catch (Exception $e) {
      drupal_set_message(t('Some thing is wrong here.'), 'error');
    }
  }

  return $ms_id;
}

/**
 * Get records for give ms id.
 *
 * @param int $ms_id
 *   Having ms id.
 *
 * @return array
 *   Having existing records for given ms id.
 */
function multiple_sitemap_get_record($ms_id = NULL) {
  $records = array();
  if (!(is_null($ms_id))) {
    $query = db_select('multiple_sitemap', 'ms');
    $query->fields('ms');
    $query->condition('ms_id', $ms_id, '=');
    $result = $query->execute()->fetchAssoc();
    if (!empty($result)) {
      $records = $result;
    }

    $record_types = array('content', 'menu', 'vocab');
    // $ms_id = db_insert('multiple_sitemap_' . $record_type)
    foreach ($record_types as $record_type) {
      $subrecords = array();
      $query = db_select('multiple_sitemap_' . $record_type, 'ms');
      $query->fields('ms');
      $query->condition('target_ms_id', $ms_id, '=');
      $results = $query->execute();
      if (!empty($results)) {
        foreach ($results as $result) {
          $subrecords[] = $result;
        }
      }

      $records[$record_type] = $subrecords;
    }
  }

  return $records;
}

/**
 * Insert new sub records for content type, menu type and vocab type.
 *
 * @param string $record_type
 *   Record type (content, menu, vocab).
 * @param int $ms_target_id
 *   Multiple sitemap id.
 * @param array $subrecords
 *   Having sub records.
 */
function multiple_sitemap_save_sub_record($record_type = NULL, $ms_target_id = NULL, $subrecords = array()) {

  // Insert new records.
  if (!is_null($ms_target_id) && !is_null($record_type) && !empty($subrecords)) {

    // Record types.
    $record_types = array('content', 'menu', 'vocab');

    // Check existence of record type.
    if (in_array($record_type, $record_types)) {
      foreach ($subrecords as $subrecord) {
        // Insert each record.
        try {
          db_insert('multiple_sitemap_' . $record_type)
            ->fields(array(
              'target_ms_id' => $ms_target_id,
              $record_type . '_type' => $subrecord['name'],
              'priority' => $subrecord['priority'],
              'changefreq' => $subrecord['changefreq'],
            ))
            ->execute();
        }
        catch (Exception $e) {
          drupal_set_message(t('Record already exist'), 'error');
        }
      }
    }
    else {
      drupal_set_message(t('Recors type does not exist'), 'error');
    }
  }
}

/**
 * Delete the sub records.
 *
 * @param string $record_type
 *   Record type name.
 * @param int $ms_target_id
 *   Target ms id.
 */
function multiple_sitemap_delete_sub_record($record_type = NULL, $ms_target_id = NULL) {
  // Insert new records.
  if (!is_null($ms_target_id) && !is_null($record_type)) {

    // Record types.
    $record_types = array('content', 'menu', 'vocab');

    // Check existence of record type.
    if (in_array($record_type, $record_types)) {
      try {

        db_delete('multiple_sitemap_' . $record_type)
          ->condition('target_ms_id', $ms_target_id, '=')
          ->execute();
      }
      catch (Exception $e) {
        drupal_set_message(t('Record is not deleted'), 'error');
      }
    }
  }
}

/**
 * Get all files name.
 *
 * @return array
 *   Having all file name.
 */
function multiple_sitemap_get_files_name() {
  $records = array();
  $query = db_select('multiple_sitemap', 'ms');
  $query->fields('ms', array('ms_id', 'file_name'));
  $results = $query->execute();
  if (!empty($results)) {
    foreach ($results as $result) {
      $records[$result->ms_id] = $result->file_name;
    }
  }

  return $records;
}

/**
 * Get links from content types.
 *
 * @param array $contents
 *   Having content details.
 *
 * @return array
 *   Having content links.
 */
function multiple_sitemap_get_content_links($contents = array()) {

  $content_links = array();

  $i = 0;

  if (!empty($contents)) {
    foreach ($contents as $content) {
      $type = $content->content_type;

      // Get the all links for given type.
      $query = db_select('node', 'n');
      $query->fields('n', array('nid'));
      $query->condition('type', $type);
      $query->condition('status', 1);

      $results = $query->execute();
      if (!empty($results)) {
        foreach ($results as $result) {
          $nid = $result->nid;
          $path = drupal_get_path_alias('node/' . $nid);
          $path = url($path, array('absolute' => TRUE));
          $content_links[$i]['link'] = $path;
          $content_links[$i]['priority'] = $content->priority;
          $content_links[$i]['changefreq'] = $content->changefreq;

          $i++;
        }
      }
    }
  }

  return $content_links;
}

/**
 * Get links from menu types.
 *
 * @param array $menus
 *   Having menu details.
 *
 * @return array
 *   Having menus links.
 */
function multiple_sitemap_get_menu_links($menus = array()) {

  $menu_links = array();
  $i = 0;

  if (!empty($menus)) {

    foreach ($menus as $menu) {
      $type = $menu->menu_type;

      $query = db_select('menu_links', 'ml');
      $query->fields('ml', array('link_path'));
      $query->condition('menu_name', $type, '<>');
      $query->condition('hidden', '0', '=');
      $query->condition('link_path', '%' . db_like('%') . '%', 'NOT LIKE');
      $query->condition('link_path', '%' . db_like('<') . '%', 'NOT LIKE');
      $results = $query->execute();
      if (!empty($results)) {
        foreach ($results as $value) {
          $path = drupal_get_path_alias($value->link_path);
          $path = url($path, array('absolute' => TRUE));
          $menu_links[$i]['link'] = $path;
          $menu_links[$i]['priority'] = $menu->priority;
          $menu_links[$i]['changefreq'] = $menu->changefreq;

          $i++;
        }
      }
    }
  }

  return $menu_links;
}

/**
 * Get links from vocab types.
 *
 * @param array $vocabs
 *   Having vocab details.
 *
 * @return array
 *   Having vocab links.
 */
function multiple_sitemap_get_vocab_links($vocabs = array()) {

  $vocab_links = array();

  $i = 0;

  if (!empty($vocabs)) {
    foreach ($vocabs as $vocab) {
      $type = $vocab->vocab_type;
      $vocabulary = taxonomy_vocabulary_machine_name_load($type);
      $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));

      foreach ($terms as $term) {
        $path = drupal_get_path_alias('taxonomy/term/' . $term->tid);
        $path = url($path, array('absolute' => TRUE));
        $vocab_links[$i]['link'] = $path;
        $vocab_links[$i]['priority'] = $vocab->priority;
        $vocab_links[$i]['changefreq'] = $vocab->changefreq;

        $i++;
      }
    }
  }

  return $vocab_links;
}

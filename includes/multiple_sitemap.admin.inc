<?php

/**
 * @file
 * Contain admin function for multiple sitemap.
 */

module_load_include('inc', 'multiple_sitemap', 'includes/multiple_sitemap.db');

/**
 * Form callback.
 */
function multiple_sitemap_add_sitemap($form, &$form_state, $arguments = array()) {
  $form['file_name'] = array(
    '#title' => t('Filename'),
    '#type' => 'textfield',
    '#default_value' => isset($arguments['file_name']) ? $arguments['file_name'] : '',
    '#description' => t('Enter file name without xml extension.Allowed only "a-z, - and _"'),
    '#required' => TRUE,
  );

  // Multiple custom links.
  $form['custom_links'] = array(
    '#title' => t('Custom links'),
    '#type' => 'textarea',
    '#default_value' => isset($arguments['custom_links']) ? $arguments['custom_links'] : '',
    '#description' => t('You can provides multiple custom links by comma-separated.'),
  );

  // Get the entity types.
  $content_types = multiple_sitemap_get_node_types();
  $menu_types = menu_get_menus();
  $vocab_types = multiple_sitemap_get_vocab_types();

  // Created an array, for iteration.
  $entities = array(
    0 => array('entity_type' => 'content', 'types' => $content_types),
    1 => array('entity_type' => 'menu', 'types' => $menu_types),
    2 => array('entity_type' => 'vocab', 'types' => $vocab_types),
  );

  foreach ($entities as $value) {

    multiple_sitemap_create_tabular_checkbox_fields($form, $value['types'], $value['entity_type'], $arguments);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form_state['ms_id'] = isset($arguments['ms_id']) ? $arguments['ms_id'] : NULL;

  return $form;
}

/**
 * Validate handler for form.
 */
function multiple_sitemap_add_sitemap_validate(&$form, &$form_state) {

  // Validate file name.
  $file_name = isset($form_state['values']['file_name']) ? $form_state['values']['file_name'] : NULL;

  if (is_null($file_name)) {
    form_set_error('file_name', t('Please provide file name'));
  }
  else {
    if (preg_match('/[^a-z_\-]/i', $file_name)) {
      form_set_error('file_name', t('Please provide file name in right format, allowed only a-z,_and -'));
    }
  }

  // Validate custom links.
  $custom_links = isset($form_state['values']['custom_links']) ? $form_state['values']['custom_links'] : '';
  $custom_links = trim($custom_links);
  if ($custom_links !== '') {
    $custom_links = explode(',', $custom_links);
    foreach ($custom_links as $custom_link) {
      if (!valid_url($custom_link)) {
        form_set_error('custom_links', t('Your links are not valid, only comma separated values are allowed'));
      }
    }
  }

}

/**
 * Submit handler for form.
 */
function multiple_sitemap_add_sitemap_submit(&$form, &$form_state) {

  $file_name = isset($form_state['values']['file_name']) ? $form_state['values']['file_name'] : NULL;

  $selected_ct = array();

  // Get selected content types.
  $content_types = $form_state['values']['content'];
  foreach ($content_types as $i => $content_type) {
    foreach ($content_type['content_type'] as $key => $value) {
      if ($key === $value) {

        $priority = isset($content_type['priority']) ? $content_type['priority'] : '0.5';
        $changefreq = isset($content_type['changefreq']) ? $content_type['changefreq'] : 'monthly';

        $selected_ct[$i]['name'] = $value;
        $selected_ct[$i]['priority'] = $priority;
        $selected_ct[$i]['changefreq'] = $changefreq;
      }
    }
  }

  $selected_menu = array();

  // Get selected menu types.
  $menu_types = $form_state['values']['menu'];
  foreach ($menu_types as $i => $menu_type) {
    foreach ($menu_type['menu_type'] as $key => $value) {
      if ($key === $value) {

        $priority = isset($menu_type['priority']) ? $menu_type['priority'] : '0.5';
        $changefreq = isset($menu_type['changefreq']) ? $menu_type['changefreq'] : 'monthly';

        $selected_menu[$i]['name'] = $value;
        $selected_menu[$i]['priority'] = $priority;
        $selected_menu[$i]['changefreq'] = $changefreq;
      }
    }
  }

  $selected_vocab = array();

  // Get selected vocab types.
  $vocab_types = $form_state['values']['vocab'];
  foreach ($vocab_types as $i => $vocab_type) {
    foreach ($vocab_type['vocab_type'] as $key => $value) {
      if ($key === $value) {

        $priority = isset($vocab_type['priority']) ? $vocab_type['priority'] : '0.5';
        $changefreq = isset($vocab_type['changefreq']) ? $vocab_type['changefreq'] : 'monthly';

        $selected_vocab[$i]['name'] = $value;
        $selected_vocab[$i]['priority'] = $priority;
        $selected_vocab[$i]['changefreq'] = $changefreq;
      }
    }
  }

  $custom_links = isset($form_state['values']['custom_links']) ? $form_state['values']['custom_links'] : NULL;

  $update_ms_id = $form_state['ms_id'];

  $input['file_name'] = $file_name;
  $input['custom_links'] = $custom_links;

  $ms_id = multiple_sitemap_save_record($input, $update_ms_id);

  if (!empty($selected_ct)) {
    multiple_sitemap_delete_sub_record('content', $ms_id);
    multiple_sitemap_save_sub_record('content', $ms_id, $selected_ct);
  }
  if (!empty($selected_menu)) {
    multiple_sitemap_delete_sub_record('menu', $ms_id);
    multiple_sitemap_save_sub_record('menu', $ms_id, $selected_menu);
  }
  if (!empty($selected_vocab)) {
    multiple_sitemap_delete_sub_record('vocab', $ms_id);
    multiple_sitemap_save_sub_record('vocab', $ms_id, $selected_vocab);
  }

  $url = 'admin/config/search/multiple-sitemap/dashboard';
  drupal_goto($url);
}

/**
 * Return all priority values.
 *
 * @return array
 *   Having priority values.
 */
function multiple_sitemap_get_priority_options() {

  $priority = array(
    '0.1' => '0.1',
    '0.2' => '0.2',
    '0.3' => '0.3',
    '0.4' => '0.4',
    '0.5' => '0.5',
    '0.6' => '0.6',
    '0.7' => '0.7',
    '0.8' => '0.8',
    '0.9' => '0.9',
    '1.0' => '1.0',
  );

  return $priority;
}

/**
 * Return all changefreq values.
 *
 * @return array
 *   Having changefreq values.
 */
function multiple_sitemap_get_changefreq_options() {

  $changefreq = array(
    'always' => 'always',
    'hourly' => 'hourly',
    'daily' => 'daily',
    'weekly' => 'weekly',
    'monthly' => 'monthly',
    'yearly' => 'yearly',
    'never' => 'never',
  );

  return $changefreq;
}

/**
 * Get the content type name in the desired format.
 *
 * @return array
 *   Content Type name in the desired format.
 */
function multiple_sitemap_get_node_types() {
  $types = array();
  $content_types = node_type_get_types();
  foreach ($content_types as $type => $details) {
    $types[$type] = $details->name;
  }
  return $types;
}

/**
 * Get the vocab type name in the desired format.
 *
 * @return array
 *   Vocab name name in the desired format.
 */
function multiple_sitemap_get_vocab_types() {
  $types = array();
  $vocabs_types = taxonomy_vocabulary_get_names();
  foreach ($vocabs_types as $type => $details) {
    $types[$type] = $details->name;
  }
  return $types;
}

/**
 * Dashboard for multiple sitemap.
 *
 * @return html
 *   Having details about created sitemaps.
 */
function multiple_sitemap_dashboard() {
  $output = t('Your installation of multiple sitemap module  is not correct.');
  if (db_table_exists('multiple_sitemap')) {
    $output = t('You have not created any sitemap yet.');
    $results = db_select('multiple_sitemap', 'ms')
      ->fields('ms')
      ->execute()
      ->fetchAll();

    $header = array(
      'File Name',
      'Custom links',
      'Edit',
      'Delete',
    );

    $rows = array();
    if (!empty($results)) {
      foreach ($results as $key => $result) {
        $ms_id = $result->ms_id;
        // $rows[$key]['msid'] = $result->ms_id.
        $rows[$key]['fname'] = $result->file_name;
        $rows[$key]['custom_links'] = $result->custom_links;

        $delete_path = '/admin/config/search/multiple-sitemap/delete/';
        $edit_path = '/admin/config/search/multiple-sitemap/edit/';

        // Add webform query.
        $delete_path .= $ms_id;
        $edit_path .= $ms_id;
        $rows[$key]['edit'] = l(t('Edit'), $edit_path);
        $rows[$key]['delete'] = l(t('Delete'), $delete_path);
      }

      return theme('table', array('header' => $header, 'rows' => $rows));
    }
  }

  return $output;
}

/**
 * Callback function for edit the sitemap.
 *
 * @param int $ms_id
 *   Multiple site map id.
 *
 * @return html
 *   Having drupal edit form.
 */
function multiple_sitemap_edit_sitemap($ms_id) {

  if (!(is_numeric($ms_id) && $ms_id > 0)) {

    return drupal_set_message(t('sitemap id is not exist'), 'error');
  }
  else {

    $records = multiple_sitemap_get_record($ms_id);

    $form = drupal_get_form('multiple_sitemap_add_sitemap', $records);

    return drupal_render($form);
  }
}

/**
 * Callbeck function for delete record.
 *
 * @param int $ms_id
 *   Map id.
 *
 * @return array
 *   Having form html.
 */
function multiple_sitemap_delete_sitemap($ms_id) {
  $form = drupal_get_form('multiple_sitemap_delete_sitemap_page', $ms_id);
  return $form;
}

/**
 * Callback function for delete confirm page.
 *
 * @param int $ms_id
 *   Having arguments which is require for deleting a record.
 */
function multiple_sitemap_delete_sitemap_page($form, &$form_state, $ms_id) {

  $form['warning'] = array(
    '#markup' => '<div><strong>' . t('Are you sure? This action can not be undone.') . '</strong></div>',
  );

  $form['actions']['delete'] = array(
    '#type'   => 'submit',
    '#value'  => t('Delete'),
    '#submit' => array('multiple_sitemap_callback_for_delete_button'),
    '#limit_validation_errors' => array(),
  );

  $form['actions']['cancel'] = array(
    '#type'   => 'submit',
    '#value'  => t('Cancel'),
    '#submit' => array('multiple_sitemap_callback_for_cancel_button'),
    '#limit_validation_errors' => array(),
  );

  $form_state['storage']['ms_id'] = $ms_id;

  return $form;
}

/**
 * Delete callback.
 */
function multiple_sitemap_callback_for_delete_button($form, &$form_state) {
  $ms_id = $form_state['storage']['ms_id'];

  $deleted = db_delete('multiple_sitemap')
    ->condition('ms_id', $ms_id)
    ->execute();

  $record_types = array('content', 'menu', 'vocab');

  foreach ($record_types as $record_type) {
    multiple_sitemap_delete_sub_record($record_type, $ms_id);
  }

  $url = 'admin/config/search/multiple-sitemap/dashboard';
  if ($deleted) {
    drupal_set_message(t('Successfully deleted'), 'status');
  }
  else {
    drupal_set_message(t('Not  deleted'), 'error');
  }
  drupal_goto($url);
}

/**
 * Callback function for cancel button & redirects.
 */
function multiple_sitemap_callback_for_cancel_button() {
  $url = 'admin/config/search/multiple-sitemap/dashboard';
  drupal_goto($url);
}

/**
 * Get form checkbox element.
 *
 * @param object &$form
 *   Having form reference.
 * @param array $types
 *   Having specific entity type.
 * @param string $entity_type
 *   Entity type.
 * @param array $arguments
 *   Having edit form argument.
 */
function multiple_sitemap_create_tabular_checkbox_fields(&$form, $types = array(), $entity_type, $arguments = array()) {

  $setvalues = array();
  if (!empty($arguments[$entity_type])) {
    $records = $arguments[$entity_type];
    foreach ($records as $key => $record) {
      $entitytype = $entity_type . '_type';
      $setvalues[$record->$entitytype]['priority'] = $record->priority;
      $setvalues[$record->$entitytype]['changefreq'] = $record->changefreq;
    }
  }

  // Form container element.
  $form['multiple_siteamp_' . $entity_type . '_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('@entity_type type settings', array('@entity_type' => $entity_type)),
  );

  $form['multiple_siteamp_' . $entity_type . '_container'][$entity_type] = array(
    '#prefix' => '<div id="multiple_siteamp_"' . $entity_type . '"_types">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => array(t('@entity_type Type', array('@entity_type' => $entity_type)), t('Priority'), t('Frequency')),
    '#rows' => array(),
  );

  $priority_list = multiple_sitemap_get_priority_options();

  $changefreq_list = multiple_sitemap_get_changefreq_options();

  $setvalueskeys = array_keys($setvalues);
  foreach ($types as $key => $type) {

    // Set default values.
    $default_values = array();

    if (in_array($key, $setvalueskeys)) {
      $default_values['type'] = $key;
      $default_values['priority'] = $setvalues[$key]['priority'];
      $default_values['changefreq'] = $setvalues[$key]['changefreq'];
    }

    // Build the fields for this row in the table. We'll be adding
    // these to the form several times, so it's easier if they are
    // individual variables rather than in an array.
    $option = array($key => $key);
    $entity = array(
      '#id' => 'ms_' . $entity_type . '_' . $key,
      '#type' => 'checkboxes',
      '#options' => $option,
      '#default_value' => !empty($default_values) ? array($default_values['type']) : array(),
      '#title' => '',
    );

    $priority = array(
      '#id' => 'msp_' . $entity_type . '_' . $key,
      '#type' => 'select',
      '#default_value' => isset($default_values['priority']) ? $default_values['priority'] : "0.5",
      '#options' => $priority_list,
    );

    $changefreq = array(
      '#id' => 'msf_' . $entity_type . '_' . $key,
      '#type' => 'select',
      '#default_value' => isset($default_values['changefreq']) ? $default_values['changefreq'] : "monthly",
      '#options' => $changefreq_list,
    );

    // Include the fields so they'll be rendered and named
    // correctly, but they'll be ignored here when rendering as
    // we're using #theme => table.
    // Note that we're using references to the variables, not just
    // copying the values into the array.
    $form['multiple_siteamp_' . $entity_type . '_container'][$entity_type][] = array(
      $entity_type . '_type' => &$entity,
      'priority' => &$priority,
      'changefreq' => &$changefreq,
    );

    // Now add references to the fields to the rows that
    // `theme_table()` will use.
    // As we've used references, the table will use the very same
    // field arrays as the FAPI used above.
    $form['multiple_siteamp_' . $entity_type . '_container'][$entity_type]['#rows'][] = array(
      array('data' => &$entity),
      array('data' => &$priority),
      array('data' => &$changefreq),
    );

    // Because we've used references we need to `unset()` our
    // variables. If we don't then every iteration of the loop will
    // just overwrite the variables we created the first time
    // through leaving us with a form with 3 copies of the same fields.
    unset($entity);
    unset($priority);
    unset($changefreq);
    unset($option);
  }
}
